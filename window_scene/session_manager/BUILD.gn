# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("../../windowmanager_aafwk.gni")

config("session_manager_public_config") {
  visibility = [ ":*" ]

  include_dirs = [
    "${window_base_path}/interfaces/innerkits/wm",
    "${window_base_path}/interfaces/include",
    "${window_base_path}/window_scene",
    "${window_base_path}/window_scene/common/include",
    "${window_base_path}/window_scene/interfaces/include",
    "${window_base_path}/window_scene/session_manager/include",
    "${window_base_path}/window_scene/session_manager/include/zidl",
    "${window_base_path}/window_scene/session_manager_service/include",

    # for session_manager
    "${window_base_path}/wmserver/include",
    "${window_base_path}/wmserver/include/zidl",

    # for screen_session_manager
    "${window_base_path}/utils/include",
    "${window_base_path}/dm/include",
    "${window_base_path}/interfaces/innerkits/dm",
    "${window_base_path}/dmserver/include",

    # for scene session manager
    "${window_base_path}/interfaces/innerkits/wm",
    "${window_base_path}/wmserver/include",
    "${window_base_path}/window_scene/screen_session_manager/include",

    # for window_manager_hilog
    "${window_base_path}/utils/include",

    "${window_base_path}/wm/include",
    "${window_base_path}/wm/include/zidl",
  ]
}

config("for_scene_session_manager_public_config") {
  visibility = [ ":*" ]

  include_dirs = [ "${resourceschedule_path}/ffrt/interfaces/kits" ]
}

ohos_shared_library("scene_session_manager") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    cfi_vcal_icall_only = true
    debug = false
  }
  sources = [
    "../../wm/src/zidl/window_manager_agent_proxy.cpp",
    "src/anomaly_detection.cpp",
    "src/distributed_client.cpp",
    "src/extension_session_manager.cpp",
    "src/scb_session_handler.cpp",
    "src/scene_input_manager.cpp",
    "src/scene_session_converter.cpp",
    "src/scene_session_dirty_manager.cpp",
    "src/scene_session_manager.cpp",
    "src/scene_session_manager_lite.cpp",
    "src/session_listener_controller.cpp",
    "src/session_manager_agent_controller.cpp",
    "src/window_scene_config.cpp",
    "src/zidl/scene_session_manager_lite_stub.cpp",
    "src/zidl/scene_session_manager_stub.cpp",
    "src/zidl/session_listener_proxy.cpp",
    "src/zidl/session_listener_stub.cpp",
  ]

  public_configs = [
    ":session_manager_public_config",
    ":for_scene_session_manager_public_config",
  ]

  deps = [
    "${window_base_path}/dm:libdm",
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
    "${window_base_path}/window_scene/common:window_scene_common",
    "${window_base_path}/window_scene/intention_event/service:intention_event_anr_manager",
    "${window_base_path}/window_scene/screen_session_manager:screen_session_manager_client",
    "${window_base_path}/window_scene/session:scene_session",
    "${window_base_path}/window_scene/session:screen_session",
  ]
  public_external_deps = [
    "ability_runtime:session_handler",
    "accessibility:accessibility_common",
    "graphic_2d:window_animation",
    "input:libmmi-client",
    "input:libmmi-util",
  ]
  external_deps = [
    "ability_base:configuration",
    "ability_base:session_info",
    "ability_base:want",
    "ability_runtime:ability_context_native",
    "ability_runtime:ability_deps_wrapper",
    "ability_runtime:ability_manager",
    "ability_runtime:ability_start_setting",
    "ability_runtime:app_manager",
    "ability_runtime:mission_info",
    "ability_runtime:session_handler",
    "ace_engine:ace_uicontent",
    "bundle_framework:appexecfwk_base",
    "bundle_framework:appexecfwk_core",
    "c_utils:utils",
    "config_policy:configpolicy_util",
    "dsoftbus:softbus_client",
    "eventhandler:libeventhandler",
    "ffrt:libffrt",
    "graphic_2d:librender_service_client",
    "hicollie:libhicollie",
    "hilog:libhilog",
    "hitrace:hitrace_meter",
    "image_framework:image_native",
    "init:libbegetutil",
    "input:libmmi-client",
    "ipc:ipc_single",
    "libxml2:libxml2",
    "napi:ace_napi",
    "resource_management:global_resmgr",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (defined(global_parts_info) &&
      defined(global_parts_info.resourceschedule_memmgr_override)) {
    external_deps += [ "memmgr_override:memmgrclient" ]
    defines += [ "MEMMGR_WINDOW_ENABLE" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.powermgr_power_manager)) {
    external_deps += [ "power_manager:powermgr_client" ]
    defines += [ "POWER_MANAGER_ENABLE" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.powermgr_display_manager)) {
    external_deps += [ "display_manager:displaymgr" ]
    defines += [ "POWERMGR_DISPLAY_MANAGER_ENABLE" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.resourceschedule_soc_perf)) {
    external_deps += [ "resource_schedule_service:ressched_client" ]
    defines += [ "RES_SCHED_ENABLE" ]
  }

  if (efficiency_manager_enable) {
    external_deps += [ "efficiency_manager:suspend_manager_client" ]
    defines += [ "EFFICIENCY_MANAGER_ENABLE" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.security_security_component_manager)) {
    external_deps += [ "security_component_manager:libsecurity_component_sdk" ]
    defines += [ "SECURITY_COMPONENT_MANAGER_ENABLE" ]
  }

  if (!defined(global_parts_info) ||
      defined(global_parts_info.inputmethod_imf)) {
    imf_enable = true
  } else {
    imf_enable = false
  }
  print("imf_enable: ", imf_enable)
  if (imf_enable == true) {
    external_deps += [ "imf:inputmethod_client" ]
    defines += [ "IMF_ENABLE" ]
  }

  innerapi_tags = [ "platformsdk" ]
  part_name = "window_manager"
  subsystem_name = "window"
}

ohos_shared_library("screen_session_manager") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    cfi_vcal_icall_only = true
    debug = false
  }
  sources = [
    "../../dm/src/zidl/display_manager_agent_proxy.cpp",
    "../screen_session_manager/src/zidl/screen_session_manager_client_proxy.cpp",
    "src/fold_screen_controller/dual_display_device_policy.cpp",
    "src/fold_screen_controller/dual_display_policy.cpp",
    "src/fold_screen_controller/fold_screen_controller.cpp",
    "src/fold_screen_controller/fold_screen_policy.cpp",
    "src/fold_screen_controller/fold_screen_sensor_manager.cpp",
    "src/fold_screen_controller/fold_screen_state_machine.cpp",
    "src/fold_screen_controller/sensor_fold_state_manager/dual_display_sensor_fold_state_manager.cpp",
    "src/fold_screen_controller/sensor_fold_state_manager/sensor_fold_state_manager.cpp",
    "src/fold_screen_controller/sensor_fold_state_manager/single_display_sensor_fold_state_manager.cpp",
    "src/screen_cutout_controller.cpp",
    "src/screen_rotation_property.cpp",
    "src/screen_scene_config.cpp",
    "src/screen_sensor_connector.cpp",
    "src/screen_session_dumper.cpp",
    "src/screen_session_manager.cpp",
    "src/screen_setting_helper.cpp",
    "src/screen_snapshot_ability_connection.cpp",
    "src/screen_snapshot_picker.cpp",
    "src/session_display_power_controller.cpp",
    "src/setting_observer.cpp",
    "src/setting_provider.cpp",
    "src/zidl/screen_session_manager_stub.cpp",
  ]

  cflags_cc = [ "-std=c++17" ]

  public_configs = [ ":session_manager_public_config" ]
  public_external_deps = [
    "ability_runtime:session_handler",
    "accessibility:accessibility_common",
    "input:libmmi-client",
    "input:libmmi-util",
  ]
  deps = [
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
    "${window_base_path}/window_scene/common:window_scene_common",
    "${window_base_path}/window_scene/interfaces/innerkits:libwsutils",
    "${window_base_path}/window_scene/session:screen_session",
    "${window_base_path}/wmserver:sms",
  ]

  external_deps = [
    "ability_base:base",
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:abilitykit_native",
    "ability_runtime:app_manager",
    "ability_runtime:dataobs_manager",
    "ability_runtime:extension_manager",
    "c_utils:utils",
    "config_policy:configpolicy_util",
    "data_share:datashare_common",
    "data_share:datashare_consumer",
    "eventhandler:libeventhandler",
    "graphic_2d:librender_service_client",
    "hicollie:libhicollie",
    "hilog:libhilog",
    "hisysevent:libhisysevent",
    "hitrace:hitrace_meter",
    "init:libbeget_proxy",
    "ipc:ipc_core",
    "libxml2:libxml2",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  defines = []
  if (window_manager_feature_subscribe_motion) {
    if (defined(global_parts_info) && defined(global_parts_info.msdp_motion)) {
      external_deps += [ "motion:motion_interface_native" ]
      defines += [ "WM_SUBSCRIBE_MOTION_ENABLE" ]
    }
  }

  if (defined(global_parts_info) && defined(global_parts_info.sensors_sensor)) {
    external_deps += [ "sensor:sensor_interface_native" ]
    defines += [ "SENSOR_ENABLE" ]
  }

  if (defined(global_parts_info) &&
      defined(global_parts_info.powermgr_power_manager)) {
    external_deps += [ "power_manager:powermgr_client" ]
    defines += [ "POWER_MANAGER_ENABLE" ]
  }

  if (window_manager_feature_tp_enable) {
    defines += [ "TP_FEATURE_ENABLE" ]
  }

  if (is_standard_system) {
    external_deps += [ "init:libbegetutil" ]
  } else {
    external_deps += [ "init_lite:libbegetutil" ]
  }

  innerapi_tags = [ "platformsdk" ]
  part_name = "window_manager"
  subsystem_name = "window"
}

ohos_shared_library("session_manager") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    cfi_vcal_icall_only = true
    debug = false
  }
  sources = [
    "../../wmserver/src/zidl/mock_session_manager_service_proxy.cpp",
    "../session_manager_service/src/session_manager_service_proxy.cpp",
    "src/session_manager.cpp",
    "src/zidl/scene_session_manager_proxy.cpp",
  ]

  cflags_cc = [ "-std=c++17" ]

  public_configs = [ ":session_manager_public_config" ]

  deps = [
    ":session_manager_lite",
    "${window_base_path}/utils:libwmutil",
    "${window_base_path}/utils:libwmutil_base",
  ]
  public_external_deps = [
    "ability_runtime:session_handler",
    "accessibility:accessibility_common",
    "graphic_2d:window_animation",
    "input:libmmi-client",
    "input:libmmi-util",
  ]
  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "ability_runtime:ability_manager",
    "ability_runtime:app_manager",
    "ability_runtime:mission_info",
    "c_utils:utils",
    "graphic_2d:librender_service_client",
    "hilog:libhilog",
    "image_framework:image_native",
    "ipc:ipc_single",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  innerapi_tags = [ "platformsdk_indirect" ]
  part_name = "window_manager"
  subsystem_name = "window"
}

ohos_shared_library("session_manager_lite") {
  branch_protector_ret = "pac_ret"
  sanitize = {
    cfi = true
    cfi_cross_dso = true
    cfi_vcal_icall_only = true
    debug = false
  }
  sources = [
    "../../wmserver/src/zidl/mock_session_manager_service_proxy.cpp",
    "../session_manager_service/src/session_manager_service_proxy.cpp",
    "src/session_manager_lite.cpp",
    "src/zidl/scene_session_manager_lite_proxy.cpp",
  ]

  cflags_cc = [ "-std=c++17" ]

  public_configs = [ ":session_manager_public_config" ]

  deps = [
    "${window_base_path}/utils:libwmutil_base",
    "${window_base_path}/window_scene/common:window_scene_common",
  ]

  public_external_deps = [
    "ability_runtime:session_handler",
    "accessibility:accessibility_common",
    "input:libmmi-client",
    "input:libmmi-util",
  ]

  external_deps = [
    "ability_base:session_info",
    "ability_base:want",
    "ability_runtime:mission_info",
    "c_utils:utils",
    "hilog:libhilog",
    "ipc:ipc_single",
    "samgr:samgr_proxy",
  ]

  innerapi_tags = [ "platformsdk_indirect" ]
  part_name = "window_manager"
  subsystem_name = "window"
}
